function addJQuery(callback) {
    var script = document.createElement("script");
    script.setAttribute("src", "https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"); 
	/* ou //ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js */
    script.addEventListener('load', function() {
        var script = document.createElement("script");
        script.textContent = "window.jQ=jQuery.noConflict(true);(" + callback.toString() + ")();";
        document.body.appendChild(script);
    }, false);
    document.body.appendChild(script);
}
function main(){
$(document).ready(
	loadDivFrete(),
	msg(), 
	mais(),
	menos(),
	remover(),
	);

function msg(event){
	$(document).on("click", '.botao-comprar', updateDivFrete);
	}

function mais(event){
	$(document).on("click", '.mais', updateDivFrete);
	}

function menos(event){
	$(document).on("click", '.menos', updateDivFrete);
	}
	
function remover(event){
	$(document).on("click", '.remover', updateDivFrete);
	}

function loadDivFrete() {
    	if (window.location.href != 'http://cro.agencia.pmweb.com.br/cupcakes4/checkout.html' )
    	{ 
    	$('<div id="preco-frete"></div>').insertBefore( $('.header').eq(0).css('margin-top','50px') );
    	updateDivFrete()
      
    	}
    else
    	{ 
    		updateDivFrete2() 
      	}}
    
function updateDivFrete() {
    	
		    	function arredondar(n) 
		    	{
		    return (Math.round(n * 100) / 100).toFixed(2);
				}
      
      let texto = "";
      var total = $('.subtotal').children('span').eq(1).text();
      
      if (total.length != 0)
      {
      var format = parseFloat(total.replace('R$ ', '').replace(',', '.'));
      }else
      {
      	format = 0;
      }
      
      if(format < 100) 
      {
    let falta = 100 - format;
    var valor = arredondar(falta);
    $('#preco-frete').css({
    	"background":"linear-gradient(to left, #780bab, black 0%, black " + (falta/100)*100 + "%, #780bab -0%)"
    });
    var correct = valor.replace('.', ',');
    texto = "FALTAM MAIS R$ " + correct + " PARA O FRETE SAIR DE GRAÇA!";
      }
      else {
        texto = "PARABÉNS! O FRETE É POR NOSSA CONTA.";
        $('#preco-frete').css({
        	"background":"linear-gradient(to right, #c417b4, #780bab)",
        	"font-weight":"bold"
        });
        $('.frete').text("VOCÊ GANHOU FRETE GRÁTIS.").css('color','#8A2BE2');
        $('.total').children('span').eq(1).text(total);
      }
      $('#preco-frete').text(texto);
      
    } 
    
function updateDivFrete2()
    {
    	function arredondar(n) 
    	{
    return (Math.round(n * 100) / 100).toFixed(2);
		}
		let texto = "";
	      var total2 = $('.valor-total').text();
	      if (total2.length != 0)
	      {
	    	var format2 = parseFloat(total2.replace('R$ ', '').replace(',', '.'));
    	  }else
    	  {
      		format2 = 0;
    	  }
      if(format2 > 100) 
      {
		$('.frete').text("VOCÊ GANHOU FRETE GRÁTIS.").css({
			"color":"#8A2BE2",
			"padding-top":"20px",
			"justify-content":"center",
			"align-itens":"center",
			"font-weight":"bold",
			"text-align":"center",
		});
		var final = arredondar(format2 - 10);
		var totext = final.replace('.',',');
		vft = "R$ " + totext;
		$('.valor-total').text(vft);
	  }
    }	
}
addJQuery(main);
