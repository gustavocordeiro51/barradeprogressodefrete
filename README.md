//UPDATE on: 01/05/22 22:02

Esse é o projeto para avaliação CRO | Coding da PMWEb.

Foi desenvolvida uma barra de frete progressiva que mostrará o valor restante para cumprimento do valor mínimo
elegível para recebimento do frete grátis (R$100).

O código serve em ferramentas de injeção/inserção de JavaScript e CSS em websites nos quais não se há acesso ao código fonte.

**TUTORIAL DE APLICAÇÃO.**

1. Ferramenta utilizada: Extensão Chrome User JavaScript and CSS;
2. Na ferramenta, no campo de texto ao lado esquerdo (JS), inserir o código barraJQuery.js;
3. Na ferramenta, no campo de texto ao lado direito (CSS), inserir o código barra.css.
